package com.tutorial.lab2.networking.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpClient {

    private static final Logger logger = Logger.getLogger(UdpClient.class.getName());

    public DatagramPacket send(DatagramPacket request) {
        DatagramPacket response = null;
        try {
            byte[] buffer = new byte[1024];
            DatagramSocket aSocket = new DatagramSocket();
            aSocket.send(request);
            response = new DatagramPacket(buffer, buffer.length);
            aSocket.receive(response);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return response;
    }
}
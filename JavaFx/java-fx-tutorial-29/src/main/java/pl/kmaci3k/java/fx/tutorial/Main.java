package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    @Override
    public void start(Stage primaryStage) throws Exception {
        IntegerProperty x = new SimpleIntegerProperty(3);
        IntegerProperty y = new SimpleIntegerProperty();

        y.bind(x.multiply(10));

        System.out.println("x = " + x.getValue() + ", y = " + y.getValue());

        x.setValue(9);

        System.out.println("x = " + x.getValue() + ", y = " + y.getValue());
    }

    public static void main(String[] args) {
        launch(args);
    }
}

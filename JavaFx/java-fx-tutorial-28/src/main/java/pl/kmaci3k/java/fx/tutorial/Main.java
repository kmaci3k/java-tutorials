package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;
    private Button button;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 28");

        Person person = new Person();

        person.firstNameProperty().addListener(
                (item, oldValue, newValue) -> {
                    System.out.println("Name changed to " + newValue);
                    System.out.println("firstNameProperty(): " + person.firstNameProperty());
                    System.out.println("getFirstName(): " + person.getFirstName());
                }
        );

        button = new Button("Submit");
        button.setOnAction(event -> person.setFirstName("Maciek"));

        StackPane layout = new StackPane();
        layout.getChildren().add(button);

        Scene scene = new Scene(layout, 300, 300);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

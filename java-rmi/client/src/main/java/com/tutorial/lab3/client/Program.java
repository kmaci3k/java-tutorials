package com.tutorial.lab3.client;


import com.tutorial.lab3.model.Packet;
import com.tutorial.lab3.model.Spectrum;
import com.tutorial.lab3.rmi.PacketApi;

import java.rmi.RemoteException;
import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Program {

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static void main(String[] args) {
        // args contain server hostname
        if (args.length < 1) {
            logger.severe("Usage: client <server host name>");
            System.exit(-1);
        }

        new Program().run(args[0]);
    }

    void run(String hostname) {

        PacketApi packetApi = PacketClient.build(hostname);

        try {
            savePacket(packetApi);
            queryServer(packetApi);
        } catch (RemoteException ex) {
            logger.severe(ex.getMessage());
        }
    }

    private void savePacket(PacketApi packetApi) throws RemoteException {
        final String device = "MyDevice";
        final String description = "DescriptionOfSpectrum";
        final Instant date = Instant.now();
        final int channelNo = 9;
        final String unit = "MyUnit";
        final double resolution = 0.1;
        final String[] buffer = { "data0", "data1", "data2" };

        Spectrum<String> spectrum = new Spectrum<>(
                device,
                description,
                date.getEpochSecond(),
                channelNo,
                unit,
                resolution,
                buffer);

        packetApi.save(spectrum);
    }

    private void queryServer(PacketApi packetApi) throws RemoteException {
        final String device = "MyDevice";
        final String description = "DescriptionOfSpectrum";
        final Instant date = Instant.now();

        List<Spectrum> responseBody = packetApi.querySpectrumByDeviceDescriptionAndDate(device, description, date);
        String responseBodyTxt = responseBody.stream().map(Packet::toString).collect(Collectors.joining("\n"));
        logger.info("Query result: " + responseBodyTxt);
    }
}

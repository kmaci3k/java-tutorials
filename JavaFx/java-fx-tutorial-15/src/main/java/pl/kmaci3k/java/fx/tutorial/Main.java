package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 15");

        Button button = new Button("Submit");

        ListView<String> listView = new ListView<>();
        listView.getItems().addAll("Iron Man", "Titanic", "Contact", "Surrogates");
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        button.setOnAction(event -> buttonClicked(listView));

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20));
        layout.getChildren().addAll(listView, button);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }

    private void buttonClicked(ListView<String> listView) {
        StringBuilder builder = new StringBuilder();
        ObservableList<String> movies = listView.getSelectionModel().getSelectedItems();

        for(String movie : movies) {
            builder.append(movie);
            builder.append("\n");
        }

        System.out.println(builder.toString());
    }

    public static void main(String[] args) {
        launch(args);
    }
}

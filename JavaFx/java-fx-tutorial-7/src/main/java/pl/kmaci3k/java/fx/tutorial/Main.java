package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;
    private Button button;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        window.setTitle("JavaFX - tutorial 7");
        window.setOnCloseRequest(this::closeProgram);

        button = new Button();
        button.setText("Close program");
        button.setOnAction(this::closeProgram);

        StackPane layout = new StackPane();
        layout.getChildren().add(button);

        Scene primaryScene = new Scene(layout, 300, 300);
        window.setScene(primaryScene);

        window.show();
    }

    private void closeProgram(Event event) {
        boolean answer = ConfirmBox.display("Exit", "Are you sure you want to exit ?");
        if(answer) {
            Platform.exit();
        }
        event.consume();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Button button;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("JavaFX - tutorial 6");

        button = new Button();
        button.setText("Click me !!!");

        // Handle event
        button.setOnAction(event -> {
            if(event.getSource().equals(button)) {
                boolean result = ConfirmBox.display("JavaFx - tutorial 6", "Are you sure ?");
                System.out.print(result);
            }
        });

        StackPane layout = new StackPane();
        layout.getChildren().add(button);

        Scene primaryScene = new Scene(layout, 300, 300);
        primaryStage.setScene(primaryScene);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

package pl.kmaci3k.gol;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

class Plansza extends JPanel implements ComponentListener, MouseListener, MouseMotionListener, Runnable {
    private static final long serialVersionUID = 1L;
    private Dimension rozmiarPlanszy = null;
    private ArrayList<Point> listaKomorek = new ArrayList<Point>(0);
    private int rozmiarPojedynczejKomorki = GraWZycie.KOMORKA_ROZMIAR;

    public Plansza() {
        addComponentListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void dodajPunktDoListy(int x, int y) {
        if (!listaKomorek.contains(new Point(x, y))) {
            listaKomorek.add(new Point(x, y));
        }
        repaint();
    }

    public void dodajPunktMyszka(MouseEvent zdarzenie) {
        int x = zdarzenie.getPoint().x / rozmiarPojedynczejKomorki - 1;
        int y = zdarzenie.getPoint().y / rozmiarPojedynczejKomorki - 1;

        if (GraWZycie.komorkaAktywna) {
            if ((x >= 0) && (x < rozmiarPlanszy.width) && (y >= 0) && (y < rozmiarPlanszy.height)) {
                dodajPunktDoListy(x, y);
            }
        } else if (GraWZycie.oscylatorAktywny) {
            if ((x >= 0) && (x < rozmiarPlanszy.width) && (y >= 0) && (y < rozmiarPlanszy.height - 2)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x, y + 1);
                dodajPunktDoListy(x, y + 2);
            }
        } else if (GraWZycie.stalaAktywna) {
            if ((x >= 1) && (x < rozmiarPlanszy.width - 2) && (y >= 0) && (y < rozmiarPlanszy.height - 2)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x + 1, y);
                dodajPunktDoListy(x - 1, y + 1);
                dodajPunktDoListy(x + 2, y + 1);
                dodajPunktDoListy(x, y + 2);
                dodajPunktDoListy(x + 1, y + 2);
            }
        } else if (GraWZycie.gliderAktywny) {
            if ((x >= 0) && (x < rozmiarPlanszy.width - 2) && (y >= 0) && (y < rozmiarPlanszy.height - 2)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x + 1, y);
                dodajPunktDoListy(x + 2, y);
                dodajPunktDoListy(x, y + 1);
                dodajPunktDoListy(x + 1, y + 2);
            }
        } else if (GraWZycie.kwadratAktywny) {
            if ((x >= 0) && (x < rozmiarPlanszy.width - 1) && (y >= 0) && (y < rozmiarPlanszy.height - 1)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x + 1, y);
                dodajPunktDoListy(x, y + 1);
                dodajPunktDoListy(x + 1, y + 1);
            }
        } else if (GraWZycie.exploderAktywny) {
            if ((x >= 0) && (x < rozmiarPlanszy.width - 4) && (y >= 0) && (y < rozmiarPlanszy.height - 4)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x + 2, y);
                dodajPunktDoListy(x + 4, y);
                dodajPunktDoListy(x, y + 1);
                dodajPunktDoListy(x, y + 2);
                dodajPunktDoListy(x, y + 3);
                dodajPunktDoListy(x, y + 4);
                dodajPunktDoListy(x + 4, y + 1);
                dodajPunktDoListy(x + 2, y + 4);
                dodajPunktDoListy(x + 4, y + 1);
                dodajPunktDoListy(x + 4, y + 2);
                dodajPunktDoListy(x + 4, y + 3);
                dodajPunktDoListy(x + 4, y + 4);
            }
        } else if (GraWZycie.tumblerAktywny) {
            if ((x >= 1) && (x < rozmiarPlanszy.width - 5) && (y >= 0) && (y < rozmiarPlanszy.height - 5)) {
                dodajPunktDoListy(x, y);
                dodajPunktDoListy(x + 1, y);
                dodajPunktDoListy(x + 3, y);
                dodajPunktDoListy(x + 4, y);
                dodajPunktDoListy(x, y + 1);
                dodajPunktDoListy(x + 1, y + 1);
                dodajPunktDoListy(x + 3, y + 1);
                dodajPunktDoListy(x + 4, y + 1);
                dodajPunktDoListy(x + 1, y + 2);
                dodajPunktDoListy(x + 1, y + 3);
                dodajPunktDoListy(x + 1, y + 4);
                dodajPunktDoListy(x + 3, y + 2);
                dodajPunktDoListy(x + 3, y + 3);
                dodajPunktDoListy(x + 3, y + 4);
                dodajPunktDoListy(x - 1, y + 3);
                dodajPunktDoListy(x - 1, y + 4);
                dodajPunktDoListy(x - 1, y + 5);
                dodajPunktDoListy(x, y + 5);
                dodajPunktDoListy(x + 5, y + 3);
                dodajPunktDoListy(x + 5, y + 4);
                dodajPunktDoListy(x + 5, y + 5);
                dodajPunktDoListy(x + 4, y + 5);
            }
        }
    }


    public void usunPunktMyszka(MouseEvent me) {
        int x = me.getPoint().x / rozmiarPojedynczejKomorki - 1;
        int y = me.getPoint().y / rozmiarPojedynczejKomorki - 1;
        if ((x >= 0) && (x < rozmiarPlanszy.width) && (y >= 0) && (y < rozmiarPlanszy.height)) {
            usunStrukturyMyszka(x, y);
        }
    }

    public void usunStrukturyMyszka(int x, int y) {
        if (GraWZycie.komorkaAktywna) {
            listaKomorek.remove(new Point(x, y));
        } else if (GraWZycie.oscylatorAktywny) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x, y + 1));
            listaKomorek.remove(new Point(x, y + 2));
        } else if (GraWZycie.stalaAktywna) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 1, y));
            listaKomorek.remove(new Point(x - 1, y + 1));
            listaKomorek.remove(new Point(x + 2, y + 1));
            listaKomorek.remove(new Point(x, y + 2));
            listaKomorek.remove(new Point(x + 1, y + 2));
        } else if (GraWZycie.gliderAktywny) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 1, y));
            listaKomorek.remove(new Point(x + 2, y));
            listaKomorek.remove(new Point(x, y + 1));
            listaKomorek.remove(new Point(x + 1, y + 2));
        } else if (GraWZycie.kwadratAktywny) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 1, y));
            listaKomorek.remove(new Point(x, y + 1));
            listaKomorek.remove(new Point(x + 1, y + 1));
        } else if (GraWZycie.exploderAktywny) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 2, y));
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 4, y));
            listaKomorek.remove(new Point(x, y + 1));
            listaKomorek.remove(new Point(x, y + 2));
            listaKomorek.remove(new Point(x, y + 3));
            listaKomorek.remove(new Point(x, y + 4));
            listaKomorek.remove(new Point(x + 4, y + 1));
            listaKomorek.remove(new Point(x + 2, y + 4));
            listaKomorek.remove(new Point(x + 4, y + 1));
            listaKomorek.remove(new Point(x + 4, y + 2));
            listaKomorek.remove(new Point(x + 4, y + 3));
            listaKomorek.remove(new Point(x + 4, y + 4));
        } else if (GraWZycie.tumblerAktywny) {
            listaKomorek.remove(new Point(x, y));
            listaKomorek.remove(new Point(x + 1, y));
            listaKomorek.remove(new Point(x + 3, y));
            listaKomorek.remove(new Point(x + 4, y));
            listaKomorek.remove(new Point(x, y + 1));
            listaKomorek.remove(new Point(x + 1, y + 1));
            listaKomorek.remove(new Point(x + 3, y + 1));
            listaKomorek.remove(new Point(x + 4, y + 1));
            listaKomorek.remove(new Point(x + 1, y + 2));
            listaKomorek.remove(new Point(x + 1, y + 3));
            listaKomorek.remove(new Point(x + 1, y + 4));
            listaKomorek.remove(new Point(x + 3, y + 2));
            listaKomorek.remove(new Point(x + 3, y + 3));
            listaKomorek.remove(new Point(x + 3, y + 4));
            listaKomorek.remove(new Point(x - 1, y + 3));
            listaKomorek.remove(new Point(x - 1, y + 4));
            listaKomorek.remove(new Point(x - 1, y + 5));
            listaKomorek.remove(new Point(x, y + 5));
            listaKomorek.remove(new Point(x + 5, y + 3));
            listaKomorek.remove(new Point(x + 5, y + 4));
            listaKomorek.remove(new Point(x + 5, y + 5));
            listaKomorek.remove(new Point(x + 4, y + 5));

        }
    }

    public void resetujPlansze() {
        listaKomorek.clear();

    }

    @Override
    public void paintComponent(Graphics obslugaGrafiki) {
        super.paintComponent(obslugaGrafiki);
        try {
            for (Point nowyPunkt : listaKomorek) {
                obslugaGrafiki.setColor(Color.RED);
                obslugaGrafiki.fillRect(rozmiarPojedynczejKomorki + (rozmiarPojedynczejKomorki * nowyPunkt.x),
                        rozmiarPojedynczejKomorki + (rozmiarPojedynczejKomorki * nowyPunkt.y), rozmiarPojedynczejKomorki, rozmiarPojedynczejKomorki);
            }
        } catch (ConcurrentModificationException cme) {
        }
        obslugaGrafiki.setColor(Color.BLACK);
        for (int i = 0; i <= rozmiarPlanszy.width; i++) {
            obslugaGrafiki.drawLine(((i * rozmiarPojedynczejKomorki) + rozmiarPojedynczejKomorki),
                    rozmiarPojedynczejKomorki, (i * rozmiarPojedynczejKomorki) + rozmiarPojedynczejKomorki,
                    rozmiarPojedynczejKomorki + (rozmiarPojedynczejKomorki * rozmiarPlanszy.height));
        }
        for (int i = 0; i <= rozmiarPlanszy.height; i++) {
            obslugaGrafiki.drawLine(rozmiarPojedynczejKomorki, ((i * rozmiarPojedynczejKomorki) + rozmiarPojedynczejKomorki),
                    rozmiarPojedynczejKomorki * (rozmiarPlanszy.width + 1), ((i * rozmiarPojedynczejKomorki) + rozmiarPojedynczejKomorki));
        }
    }

    @Override
    public void componentResized(ComponentEvent zdarzenie) {
        rozmiarPlanszy = new Dimension(getWidth() / rozmiarPojedynczejKomorki - 1, getHeight() / rozmiarPojedynczejKomorki - 1);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent zdarzenie) {
        dodajPunktMyszka(zdarzenie);
        if (zdarzenie.getButton() == MouseEvent.BUTTON3) {
            usunPunktMyszka(zdarzenie);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void run() {
        boolean[][] planszaGry = new boolean[rozmiarPlanszy.width + 2][rozmiarPlanszy.height + 2];
        for (Point ozywKomorke : listaKomorek) {
            planszaGry[ozywKomorke.x + 1][ozywKomorke.y + 1] = true;
        }
        ArrayList<Point> zyweKomorki = new ArrayList<Point>(0);
        for (int i = 1; i < planszaGry.length - 1; i++) {
            for (int j = 1; j < planszaGry[0].length - 1; j++) {
                int otaczajacezyweKomorki = 0;
                if (planszaGry[i - 1][j - 1]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i - 1][j]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i - 1][j + 1]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i][j - 1]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i][j + 1]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i + 1][j - 1]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i + 1][j]) {
                    otaczajacezyweKomorki++;
                }
                if (planszaGry[i + 1][j + 1]) {
                    otaczajacezyweKomorki++;
                }


                if (planszaGry[i][j]) {
                    if ((otaczajacezyweKomorki == 2) || (otaczajacezyweKomorki == 3)) {
                        zyweKomorki.add(new Point(i - 1, j - 1));
                    }
                } else {
                    if (otaczajacezyweKomorki == 3) {
                        zyweKomorki.add(new Point(i - 1, j - 1));
                    }
                }
            }
        }


        resetujPlansze();


        listaKomorek.addAll(zyweKomorki);

        if (GraWZycie.warunekAktywny) {
            for (Point komorki : zyweKomorki) {
                if ((int) komorki.getX() == rozmiarPlanszy.width - 1) {
                    komorki.x = 0;
                    komorki.y = (int) komorki.getY();
                } else if ((int) komorki.getY() == rozmiarPlanszy.height - 1) {
                    komorki.x = (int) komorki.getX();
                    komorki.y = 0;
                } else if ((int) komorki.getX() == 0) {
                    komorki.x = rozmiarPlanszy.width - 1;
                    komorki.y = (int) komorki.getY();
                } else if ((int) komorki.getY() == 0) {
                    komorki.x = (int) komorki.getX();
                    komorki.y = rozmiarPlanszy.height - 1;
                }
            }
        }
        repaint();
        try {
            Thread.sleep(1000 / GraWZycie.szybkoscGry);
            run();
        } catch (InterruptedException ex) {
        }
    }
}

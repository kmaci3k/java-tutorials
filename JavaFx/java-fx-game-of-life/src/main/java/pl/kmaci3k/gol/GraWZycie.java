package pl.kmaci3k.gol;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraWZycie extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private static final Dimension ROZMIAR_OKNA = new Dimension(1920, 1080);

	public static final int KOMORKA_ROZMIAR = 25;
	public static boolean komorkaAktywna;
	public static boolean oscylatorAktywny;
	public static boolean stalaAktywna;
	public static boolean gliderAktywny;
	public static boolean kwadratAktywny;
	public static boolean exploderAktywny;
	public static boolean tumblerAktywny;
	public static boolean warunekAktywny;
	public static Integer szybkoscGry;

	String[] strukturyGry = {"Komorka", "Oscylator", "Staza", "Glider", "Kwadrat", "Exploder", "Tumbler"};
	Integer[] szybkosciGry = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100};
	String[] warunkiGry = {"Nieperiodyczne", "Periodyczne" };
	private JMenuBar glowneMenu;
	private JPanel glownyPanel;
	private JButton przyciskGraj, przyciskPauza, przyciskRestart, przyciskWyjscie;
	private JLabel labelStruktura, labelSzybkosc, labelWarunki;
	private JComboBox listaStruktur, listaSzybkosci, listaWarunkow;
	private Plansza graPlansza;
	private Thread graj;

	public GraWZycie() {
		glowneMenu = new JMenuBar();
		glownyPanel = new JPanel();
		listaStruktur = new JComboBox(strukturyGry);
		listaSzybkosci = new JComboBox(szybkosciGry);
		listaWarunkow = new JComboBox(warunkiGry);

		przyciskGraj = new JButton("Graj");
		przyciskPauza = new JButton("Pauza");
		przyciskRestart = new JButton("Restart");
		przyciskWyjscie  = new JButton("Zamknij");
		labelStruktura = new JLabel("Wybierz strukture: ");
		labelSzybkosc = new JLabel("Wybierz szybkosc gry: ");
		labelWarunki = new JLabel("Wybierz warunek: ");

		setJMenuBar(glowneMenu);

		przyciskGraj.addActionListener(this);
		przyciskPauza.addActionListener(this);
		przyciskRestart.addActionListener(this);
		przyciskWyjscie.addActionListener(this);
		listaStruktur.addActionListener(this);
		listaSzybkosci.addActionListener(this);
		listaWarunkow.addActionListener(this);

		przyciskPauza.setEnabled(false);
		listaStruktur.setSelectedIndex(0);

		graPlansza = new Plansza();

		glownyPanel.setBackground(Color.ORANGE);
		graPlansza.setBackground(Color.LIGHT_GRAY);

		glowneMenu.add(glownyPanel);

		glownyPanel.add(przyciskGraj);
		glownyPanel.add(przyciskPauza);
		glownyPanel.add(przyciskRestart);

		glownyPanel.add(labelStruktura);
		glownyPanel.add(listaStruktur);

		glownyPanel.add(labelSzybkosc);
		glownyPanel.add(listaSzybkosci);

		glownyPanel.add(labelWarunki);
		glownyPanel.add(listaWarunkow);
		glownyPanel.add(przyciskWyjscie);

		add(graPlansza);
	}

	public void ustawGre(boolean czyGraWlaczona) {
		if (czyGraWlaczona == true) {
			przyciskGraj.setEnabled(false);
			przyciskPauza.setEnabled(true);
			szybkoscGry = (Integer) listaSzybkosci.getSelectedItem();
			graj = new Thread(graPlansza);
			graj.start();
		} else {
			przyciskGraj.setEnabled(true);
			przyciskPauza.setEnabled(false);
			graj.interrupt();
		}
	}


	@Override
	public void actionPerformed(ActionEvent zdarzenie) {
		if(zdarzenie.getSource().equals(przyciskGraj)) {
			ustawGre(true);
		} else if (zdarzenie.getSource().equals(przyciskPauza)) {
			ustawGre(false);
		} else if (zdarzenie.getSource().equals(przyciskRestart)) {
			graPlansza.resetujPlansze();
			graPlansza.repaint();
		} else if (zdarzenie.getSource().equals(przyciskWyjscie)) {
			System.exit(0);
		} else if (zdarzenie.getSource().equals(listaStruktur)) {
			if (listaStruktur.getSelectedItem().equals(strukturyGry[0])) {
			komorkaAktywna = true;
			oscylatorAktywny = false;
			stalaAktywna = false;
			gliderAktywny = false;
			kwadratAktywny = false;
			exploderAktywny = false;
			tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[1])) {
				komorkaAktywna = false;
				oscylatorAktywny = true;
				stalaAktywna = false;
				gliderAktywny = false;
				kwadratAktywny = false;
				exploderAktywny = false;
				tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[2])) {
				komorkaAktywna = false;
				oscylatorAktywny = false;
				stalaAktywna = true;
				gliderAktywny = false;
				kwadratAktywny = false;
				exploderAktywny = false;
				tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[3])) {
				komorkaAktywna = false;
				oscylatorAktywny = false;
				stalaAktywna = false;
				gliderAktywny = true;
				kwadratAktywny = false;
				exploderAktywny = false;
				tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[4])) {
				komorkaAktywna = false;
				oscylatorAktywny = false;
				stalaAktywna = false;
				gliderAktywny = false;
				kwadratAktywny = true;
				exploderAktywny = false;
				tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[5])) {
				komorkaAktywna = false;
				oscylatorAktywny = false;
				stalaAktywna = false;
				gliderAktywny = false;
				kwadratAktywny = false;
				exploderAktywny = true;
				tumblerAktywny = false;
			} else if (listaStruktur.getSelectedItem().equals(strukturyGry[6])) {
				komorkaAktywna = false;
				oscylatorAktywny = false;
				stalaAktywna = false;
				gliderAktywny = false;
				kwadratAktywny = false;
				exploderAktywny = false;
				tumblerAktywny = true;
			}
	} else if (zdarzenie.getSource().equals(listaWarunkow)) {
		if (listaWarunkow.getSelectedItem().equals(warunkiGry[0])) {
			warunekAktywny = false;
		} else if (listaWarunkow.getSelectedItem().equals(warunkiGry[1])) {
			warunekAktywny = true;
		}
	}
}

	public static void main(String[] args) {
		JFrame graWZycie = new GraWZycie();
		graWZycie.setSize(ROZMIAR_OKNA);
		graWZycie.setTitle("Gra W Zycie - Modelowanie Wieloskalowe - Adrian Bigaj");
		graWZycie.setExtendedState(JFrame.MAXIMIZED_BOTH);
		graWZycie.setResizable(false);
		graWZycie.setVisible(true);
	}






}

package com.tutorial.lab2.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Logger;

public class Serializer {

    private static final Logger logger = Logger.getLogger(Serializer.class.getName());

    public static byte[] serialize(Object obj) {
        try {
            ByteArrayOutputStream bytesStream = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bytesStream);
            out.writeObject(obj);
            out.close();
            bytesStream.close();
            return bytesStream.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            return new byte[0];
        }
    }

    public static Object deserialize(byte[] bytes) throws ClassNotFoundException {
        Object obj = null;
        try {
            ByteArrayInputStream bytesStream = new ByteArrayInputStream(bytes);
            ObjectInputStream in = new ObjectInputStream(bytesStream);
            obj = in.readObject();
            in.close();
            bytesStream.close();
        } catch (IOException ex) {
            logger.warning(ex.getMessage());
        }
        return obj;
    }
}

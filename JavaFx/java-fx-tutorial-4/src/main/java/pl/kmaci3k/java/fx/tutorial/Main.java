package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;
    private Scene scene1;
    private Scene scene2;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        Label label1 = new Label("Welcome to the first scene");
        Button button1 = new Button("Go to scene 2");
        button1.setOnAction(event -> window.setScene(scene2));

        VBox layout1 = new VBox(20);
        layout1.getChildren().addAll(label1, button1);
        scene1 = new Scene(layout1, 200, 200);

        Button button2 = new Button("This scene sucks, go to scene 2");
        button2.setOnAction(event -> window.setScene(scene1));

        StackPane layout2 = new StackPane();
        layout2.getChildren().add(button2);
        scene2 = new Scene(layout2, 600, 300);

        window.setTitle("JavaFX - tutorial 4");
        window.setScene(scene1);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

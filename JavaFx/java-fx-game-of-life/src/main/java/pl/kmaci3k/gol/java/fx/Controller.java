package pl.kmaci3k.gol.java.fx;

/**
 * Created by mklys on 31/05/16.
 */
public class Controller {

    public void startGame() {
        System.out.println("Game started");
    }

    public void pauseGame() {
        System.out.println("Game paused");
    }

    public void restartGame() {
        System.out.println("Game restarted");
    }
}

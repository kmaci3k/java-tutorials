package com.tutorial.lab3.model;

import java.io.Serializable;
import java.time.Instant;

public class Request extends Packet implements Serializable {

    private final RequestType type;

    private Request(RequestType type, String device, String description, long date) {
        super(device, description, date);
        this.type = type;
    }

    public RequestType getType() {
        return type;
    }

    public static Request by(String device, String description) {
        return new Request(RequestType.DEVICE_DESC, device, description, Instant.now().getEpochSecond());
    }

    public static Request by(String device, String description, Instant date) {
        return new Request(RequestType.DEVICE_DESC_DATE, device, description, date.getEpochSecond());
    }

    public enum RequestType {
        DEVICE_DESC,
        DEVICE_DESC_DATE
    }
}

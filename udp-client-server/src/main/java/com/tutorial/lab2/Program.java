package com.tutorial.lab2;

import com.tutorial.lab1.Packet;
import com.tutorial.lab1.Request;
import com.tutorial.lab1.Spectrum;
import com.tutorial.lab2.networking.udp.UdpClient;
import com.tutorial.lab2.utils.Serializer;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Program {

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static void main(String[] args) {
        // args contain server hostname
        if (args.length < 1) {
            logger.severe("Usage: UDPClient <server host name>");
            System.exit(-1);
        }

        new Program().run(args[0]);
    }

    void run(String hostname) {
        int port = 9876;
        savePacket(hostname, port);
        queryServer(hostname, port);
    }

    private void savePacket(String hostname, int port) {
        final String device = "MyDevice";
        final String description = "DescriptionOfSpectrum";
        final Instant date = Instant.now();
        final int channelNo = 9;
        final String unit = "MyUnit";
        final double resolution = 0.1;
        final String[] buffer = { "data0", "data1", "data2" };

        Spectrum<String> spectrum = new Spectrum<>(
                device,
                description,
                date.getEpochSecond(),
                channelNo,
                unit,
                resolution,
                buffer);

        DatagramPacket response = sendToServer(hostname, port, Serializer.serialize(spectrum));
        try {
            Packet responseBody = (Packet) Serializer.deserialize(response.getData());
            logger.info("Deserialized response: " + responseBody.toString());
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void queryServer(String hostname, int port) {
        final String device = "MyDevice";
        final String description = "DescriptionOfSpectrum";
        final Instant date = Instant.now();

        Request request = Request.by(device, description, date);
        DatagramPacket response = sendToServer(hostname, port, Serializer.serialize(request));

        try {
            List<Packet> responseBody = (List<Packet>) Serializer.deserialize(response.getData());
            String responseBodyTxt = responseBody.stream().map(Packet::toString).collect(Collectors.joining("\n"));
            logger.info("Deserialized response: " + responseBodyTxt);
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private DatagramPacket sendToServer(String hostname, int port, byte[] data) {
        DatagramPacket response = null;
        try {
            InetAddress address = InetAddress.getByName(hostname);
            DatagramPacket packet = new DatagramPacket(data, data.length, address, port);

            final UdpClient client = new UdpClient();
            response = client.send(packet);
        } catch (UnknownHostException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return response;
    }
}

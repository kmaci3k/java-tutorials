package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 10");

        //Form
        TextField nameInput = new TextField();
        Button button = new Button("Click me");
        button.setOnAction(event -> isInt(nameInput, nameInput.getText()));

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20));
        layout.getChildren().addAll(nameInput, button);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }

    private boolean isInt(TextField input, String text) {
        try {
            int age = Integer.parseInt(text);
            System.out.println("User is " + age);
            return true;
        } catch (NumberFormatException ex) {
            System.out.println("Error: " + text + " is not a number");
            return false;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

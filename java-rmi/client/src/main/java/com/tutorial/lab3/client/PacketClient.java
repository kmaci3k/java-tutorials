package com.tutorial.lab3.client;

import com.tutorial.lab3.model.Packet;
import com.tutorial.lab3.model.Spectrum;
import com.tutorial.lab3.model.TimeHistory;
import com.tutorial.lab3.rmi.PacketApi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;

public class PacketClient implements PacketApi {

    private static final Logger logger = Logger.getLogger(PacketClient.class.getName());

    private final PacketApi packetApi;

    private PacketClient(PacketApi packetApi) {
        this.packetApi = packetApi;
    }

    @Override
    public void save(Packet packet) throws RemoteException {
        packetApi.save(packet);
    }

    @Override
    public List<Spectrum> querySpectrumByDeviceAndDescription(String device, String description)throws RemoteException {
        return packetApi.querySpectrumByDeviceAndDescription(device, description);
    }

    @Override
    public List<Spectrum> querySpectrumByDeviceDescriptionAndDate(String device, String description, Instant date)throws RemoteException {
        return packetApi.querySpectrumByDeviceDescriptionAndDate(device, description, date);
    }

    @Override
    public List<TimeHistory> queryTimeHistoryByDeviceAndDescription(String device, String description)throws RemoteException {
        return packetApi.queryTimeHistoryByDeviceAndDescription(device, description);
    }

    @Override
    public List<TimeHistory> queryTimeHistoryByDeviceDescriptionAndDate(String device, String description, Instant date) throws RemoteException {
        return packetApi.queryTimeHistoryByDeviceDescriptionAndDate(device, description, date);
    }

    static PacketClient build(String hostname) {
        try {
            Registry registry = LocateRegistry.getRegistry(hostname);
            PacketApi packetApi = (PacketApi) registry.lookup("PacketServer");
            return new PacketClient(packetApi);
        } catch (RemoteException | NotBoundException ex) {
            logger.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}

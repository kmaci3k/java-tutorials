CREATE TABLE system_user
(
  id bigint NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  login character varying(255) NOT NULL,
  password character varying(255) NOT NULL,
  CONSTRAINT system_user_pkey PRIMARY KEY (id),
  CONSTRAINT uk_ouc8los1ipbb5ffcuvwhu4nsf UNIQUE (login)
)
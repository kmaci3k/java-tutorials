package com.tutorial.lab2.networking.udp;

import com.tutorial.lab1.Packet;
import com.tutorial.lab2.utils.Serializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PacketRepository {

    private static final Logger logger = Logger.getLogger(PacketRepository.class.getName());
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();
    private final ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();

    void save(Packet packet) {
        List<Packet> packets = getByDeviceAndDescription(packet);
        packets.add(packet);

        try {
            writeLock.lock();
            boolean isSerialized = serialize(packets, getFilename(packet));
            if(!isSerialized) {
                logger.warning("Spectrum was not serialized");
            }
        } finally {
            writeLock.unlock();
        }
    }

    List<Packet> getByDeviceAndDescription(Packet searchedPacket) {
        return getStreamByDeviceAndDescription(searchedPacket).collect(Collectors.toList());
    }

    List<Packet> getByDeviceDescriptionAndDate(Packet searchedPacket) {
        Instant now = Instant.ofEpochSecond(searchedPacket.getDate());
        return getStreamByDeviceAndDescription(searchedPacket)
                .filter(packet -> Instant.ofEpochSecond(packet.getDate()).isBefore(now))
                .collect(Collectors.toList());
    }

    private Stream<Packet> getStreamByDeviceAndDescription(Packet packet) {
        Path filename = Paths.get(getFilename(packet));
        if(Files.notExists(filename)) {
            return Stream.empty();
        }

        Stream<Packet> packetStream = Stream.empty();
        try {
            readLock.lock();
            packetStream = ((List<Packet>) Serializer.deserialize(Files.readAllBytes(filename))).stream();
        } catch (IOException | ClassNotFoundException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            readLock.unlock();
        }
        return packetStream;
    }

    private String getFilename(Packet packet) {
        return String.format("%s.%s", packet.getDevice(), packet.getDescription());
    }

    private boolean serialize(Object obj, String path) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();
            logger.info("Serialized data is saved in " + path);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application implements EventHandler<ActionEvent> {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Button button;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("JavaFX - tutorial 2");

        button = new Button();
        button.setText("Click me !!!");

        // Handle event
        button.setOnAction(this);

        StackPane layout = new StackPane();
        layout.getChildren().add(button);

        Scene primaryScene = new Scene(layout, 300, 300);
        primaryStage.setScene(primaryScene);

        primaryStage.show();
    }

    public void handle(ActionEvent event) {
        if(event.getSource().equals(button)) {
            System.out.println("How dare you to click me !!!");
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

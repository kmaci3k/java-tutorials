package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 14");

        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll("Apples", "Bananas", "Strawberries", "Raspberries");

        comboBox.setPromptText("What is your favourite fruit ?");
        comboBox.setEditable(true);
        comboBox.setOnAction(event -> System.out.println(comboBox.getValue()));

        Button button = new Button("Click me");
        button.setOnAction(event -> printFruit(comboBox));

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20));
        layout.getChildren().addAll(comboBox, button);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }

    private void printFruit(ComboBox<String> comboBox) {
        System.out.println(comboBox.getValue());
    }

    public static void main(String[] args) {
        launch(args);
    }
}

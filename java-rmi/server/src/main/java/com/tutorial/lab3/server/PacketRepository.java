package com.tutorial.lab3.server;

import com.tutorial.lab3.model.Packet;
import com.tutorial.lab3.model.Spectrum;
import com.tutorial.lab3.model.TimeHistory;
import com.tutorial.lab3.server.utils.Serializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PacketRepository {

    private static final Logger logger = Logger.getLogger(PacketRepository.class.getName());
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();
    private final ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();

    private static final Map<Class<? extends Packet>, String> fileExtensions = new HashMap<>();

    static {
        fileExtensions.put(Spectrum.class, "spc");
        fileExtensions.put(TimeHistory.class, "thc");
    }

    <T extends Packet> void save(T packet) {
        Class<T> clazz = (Class<T>) packet.getClass();
        List<T> packets = getByDeviceAndDescription(packet.getDevice(), packet.getDescription(), clazz);
        packets.add(packet);

        try {
            writeLock.lock();
            boolean isSerialized = serialize(packets, getFilename(packet));
            if(!isSerialized) {
                logger.warning("Spectrum was not serialized");
            }
        } finally {
            writeLock.unlock();
        }
    }

    <T extends Packet> List<T> getByDeviceAndDescription(String device,
                                           String description,
                                           Class<T> clazz) {
        return getStreamByDeviceAndDescription(device, description, clazz).collect(Collectors.toList());
    }

    <T extends Packet> List<T> getByDeviceDescriptionAndDate(String device,
                                               String description,
                                               Instant date,
                                               Class<T> clazz) {
        return getStreamByDeviceAndDescription(device, description, clazz)
                .filter(packet -> Instant.ofEpochSecond(packet.getDate()).isBefore(date))
                .collect(Collectors.toList());
    }

    private <T extends Packet> Stream<T> getStreamByDeviceAndDescription(String device,
                                                           String description,
                                                           Class<T> clazz) {
        Path filename = Paths.get(getFilename(device, description, clazz));
        if(Files.notExists(filename)) {
            return Stream.empty();
        }

        Stream<T> packetStream = Stream.empty();
        try {
            readLock.lock();
            packetStream = ((List<T>) Serializer.deserialize(Files.readAllBytes(filename))).stream();
        } catch (IOException | ClassNotFoundException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            readLock.unlock();
        }
        return packetStream;
    }

    private String getFilename(Packet packet) {
        return getFilename(packet.getDevice(), packet.getDescription(), packet.getClass());
    }

    private String getFilename(String device, String description, Class<? extends Packet> clazz) {
        return String.format("%s.%s.%s", device, description, fileExtensions.get(clazz));
    }

    private boolean serialize(Object obj, String path) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(obj);
            out.close();
            fileOut.close();
            logger.info("Serialized data is saved in " + path);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

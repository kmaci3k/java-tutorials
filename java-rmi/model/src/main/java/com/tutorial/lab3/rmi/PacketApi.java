package com.tutorial.lab3.rmi;

import com.tutorial.lab3.model.Packet;
import com.tutorial.lab3.model.Spectrum;
import com.tutorial.lab3.model.TimeHistory;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.Instant;
import java.util.List;

public interface PacketApi extends Remote {
    void save(Packet packet) throws RemoteException;;

    List<Spectrum> querySpectrumByDeviceAndDescription(String device, String description) throws RemoteException;;
    List<Spectrum> querySpectrumByDeviceDescriptionAndDate(String device, String description, Instant date) throws RemoteException;;

    List<TimeHistory> queryTimeHistoryByDeviceAndDescription(String device, String description) throws RemoteException;;
    List<TimeHistory> queryTimeHistoryByDeviceDescriptionAndDate(String device, String description, Instant date) throws RemoteException;;
}

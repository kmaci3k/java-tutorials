package com.tutorial.lab2.networking.udp;

import com.tutorial.lab1.Packet;
import com.tutorial.lab1.Request;
import com.tutorial.lab2.utils.Serializer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpServer {

    private static final Logger logger = Logger.getLogger(UdpServer.class.getName());

    public static void main(String[] args) {
        new UdpServer(9876).run();
    }

    private final DatagramSocket socket;
    private final ExecutorService executors = Executors.newFixedThreadPool(10);
    private final PacketRepository repository = new PacketRepository();

    UdpServer(int port) {
        this.socket = setup(port);
    }

    private DatagramSocket setup(int port) {
        try {
            return new DatagramSocket(port);
        } catch (SocketException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    void run() {
        try {
            byte[] buffer = new byte[1024];
            while (true) {
                DatagramPacket receivedDatagram = new DatagramPacket(buffer, buffer.length);
                logger.info("Waiting for request...");
                socket.receive(receivedDatagram);

                executors.submit(() -> handle(receivedDatagram));
                logger.info("Task scheduled...");
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            socket.close();
        }
    }

    private void handle(DatagramPacket receivedDatagram) {
        try {
            Packet receivedPacket = (Packet) Serializer.deserialize(receivedDatagram.getData());

            byte[] responseBody = receivedDatagram.getData();
            if (receivedPacket instanceof Request) {
                responseBody = handleSave((Request) receivedPacket);
            } else {
                repository.save(receivedPacket);
            }

            DatagramPacket reply = new DatagramPacket(responseBody,
                    responseBody.length,
                    receivedDatagram.getAddress(),
                    receivedDatagram.getPort());
            socket.send(reply);
        } catch (IOException | ClassNotFoundException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            logger.info("Task finished.");
        }
    }

    private byte[] handleSave(Request request) {
        List<Packet> foundPackets = Collections.emptyList();
        switch (request.getType()) {
            case DEVICE_DESC: {
                foundPackets = repository.getByDeviceAndDescription(request);
                break;
            }
            case DEVICE_DESC_DATE: {
                foundPackets = repository.getByDeviceDescriptionAndDate(request);
                break;
            }
        }
        return Serializer.serialize(foundPackets);
    }
}


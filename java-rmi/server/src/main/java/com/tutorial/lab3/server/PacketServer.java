package com.tutorial.lab3.server;

import com.tutorial.lab3.model.Packet;
import com.tutorial.lab3.model.Spectrum;
import com.tutorial.lab3.model.TimeHistory;
import com.tutorial.lab3.rmi.PacketApi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;

public class PacketServer extends UnicastRemoteObject implements PacketApi {

    private static final Logger logger = Logger.getLogger(PacketServer.class.getName());

    private final PacketRepository repository = new PacketRepository();

    protected PacketServer() throws RemoteException {
    }

    @Override
    public void save(Packet packet) {
        logger.info("Save packet to file");
        repository.save(packet);
    }

    @Override
    public List<Spectrum> querySpectrumByDeviceAndDescription(String device, String description) {
        logger.info("Query Spectrum by device and description");
        return repository.getByDeviceAndDescription(device, description, Spectrum.class);
    }

    @Override
    public List<Spectrum> querySpectrumByDeviceDescriptionAndDate(String device, String description, Instant date) {
        logger.info("Query Spectrum by device, description and date");
        return repository.getByDeviceDescriptionAndDate(device, description, date, Spectrum.class);
    }

    @Override
    public List<TimeHistory> queryTimeHistoryByDeviceAndDescription(String device, String description) {
        logger.info("Query TimeHistory by device and description");
        return repository.getByDeviceAndDescription(device, description, TimeHistory.class);
    }

    @Override
    public List<TimeHistory> queryTimeHistoryByDeviceDescriptionAndDate(String device, String description, Instant date) {
        logger.info("Query TimeHistory by device, description and date");
        return repository.getByDeviceDescriptionAndDate(device, description, date, TimeHistory.class);
    }

    static PacketApi build() {
        try {
            Registry registry = LocateRegistry.createRegistry(1099);
            PacketApi api = new PacketServer();
            registry.rebind("PacketServer", api);
            logger.info("Packet server is READY");
            return api;
        } catch(RemoteException ex) {
            logger.severe(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        PacketServer.build();
    }
}

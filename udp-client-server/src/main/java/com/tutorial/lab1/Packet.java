package com.tutorial.lab1;

import java.io.Serializable;

public abstract class Packet implements Serializable {

    public Packet(String device, String description, long date) {
        super();
        this.device = device;
        this.description = description;
        this.date = date;
    }

    private String device;
    private String description;
    private long date;

    public String getDevice() {
        return device;
    }

    public String getDescription() {
        return description;
    }

    public long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Packet [device=" + device + ", description=" + description + ", date=" + date + "]";
    }

}

package pl.kmaci3k.gol.java.fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by mklys on 31/05/16.
 */
public class GameOfLife extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent mainView = FXMLLoader.load(getClass().getResource("gof-view.fxml"));

        primaryStage.setTitle("Game Of Life");
        primaryStage.setScene(new Scene(mainView, 1024, 768));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

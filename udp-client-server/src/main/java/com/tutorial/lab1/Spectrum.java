package com.tutorial.lab1;

public class Spectrum<T> extends Sequence<T> {

	public Spectrum(String device,
                    String description,
                    long date,
                    int channelNo,
                    String unit,
                    double resolution,
                    T[] buffer) {
		super(device, description, date, channelNo, unit, resolution, buffer);
	}
}

package pl.kmaci3k.jdbc.tutorial;

import pl.kmaci3k.jdbc.tutorial.model.SystemUser;

import java.sql.*;

/**
 * Created by maciek on 15.06.16.
 */
public class Program implements AutoCloseable {

    public static void main(String[] args) {
        try (Program program = new Program()) {
            program.connect();

            SystemUser newSystemUser = new SystemUser();
            newSystemUser.setId(1L);
            newSystemUser.setFirstName("Albert");
            newSystemUser.setLastName("Einstein");
            newSystemUser.setLogin("aeinstein");
            newSystemUser.setPassword("e=mc^2");

            program.addUser(newSystemUser);
            program.printAllUsers();
        }
    }

    private Connection connection;

    public void connect() {
        if(connection == null) {
            connection = connectImpl();
        }
    }

    private Connection connectImpl() {
        try {
            Class.forName("org.postgresql.Driver");

            String url = "jdbc:postgresql://localhost:5432/jdbc-tutorial";
            String user = "postgres";
            String password = "postgres";

            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void addUser(SystemUser systemUser) {
        try {
            String sql = "INSERT INTO SYSTEM_USER (id, first_name, last_name, login, password) VALUES (?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, systemUser.getId());
            statement.setString(2, systemUser.getFirstName());
            statement.setString(3, systemUser.getLastName());
            statement.setString(4, systemUser.getLogin());
            statement.setString(5, systemUser.getPassword());

            boolean success = statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void printAllUsers() {
        String sql = "SELECT * FROM SYSTEM_USER;";
        try {
            Statement statement = connection.createStatement();
            ResultSet users = statement.executeQuery(sql);

            while(users.next()) {
                Long id = users.getLong(1);
                String firstName = users.getString(2);
                String lastName = users.getString(3);
                String login = users.getString(4);

                System.out.println(String.format("%d, %s %s, %s", id, firstName, lastName, login));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void close() {
        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}

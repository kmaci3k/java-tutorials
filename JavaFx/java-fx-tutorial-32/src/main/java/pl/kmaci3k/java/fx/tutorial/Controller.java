package pl.kmaci3k.java.fx.tutorial;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {

    @FXML
    private Button button;

    public void handleButtonClick(ActionEvent event) {
        System.out.println("Submit button has been clicked");
        button.setText("Submitted");
    }
}

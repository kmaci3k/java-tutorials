package com.tutorial.lab3.model;

public class Spectrum<T> extends Sequence<T> {

	public Spectrum(String device,
                    String description,
                    long date,
                    int channelNo,
                    String unit,
                    double resolution,
                    T[] buffer) {
		super(device, description, date, channelNo, unit, resolution, buffer);
	}
}

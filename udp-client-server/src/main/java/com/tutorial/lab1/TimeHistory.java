package com.tutorial.lab1;

public class TimeHistory<T> extends Sequence<T> {
	public TimeHistory(String device, String descrition, long date, int channelNr, String unit, double resolution,
			T[] buffer, double sensitivity) {
		super(device, descrition, date, channelNr, unit, resolution, buffer);
		this.sensitivity = sensitivity;
	}

	double sensitivity;

	@Override
	public String toString() {
		return "TimeHistory [sensitivity=" + sensitivity + "]";
	}

	
}

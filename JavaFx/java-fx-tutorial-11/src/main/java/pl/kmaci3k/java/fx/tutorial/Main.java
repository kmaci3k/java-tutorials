package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 11");

        CheckBox box1 = new CheckBox("Option 1");
        CheckBox box2 = new CheckBox("Option 2");
        box2.setSelected(true);

        Button button = new Button("Order");
        button.setOnAction(event -> handleOptions(box1, box2));

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20));
        layout.getChildren().addAll(box1, box2, button);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }

    private void handleOptions(CheckBox box1, CheckBox box2) {
        String message = "User selection:";
        if(box1.isSelected()) {
            message += "Option 1 is selected\n";
        }
        if(box2.isSelected()) {
            message += "Option 2 is selected\n";
        }
        System.out.println(message);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

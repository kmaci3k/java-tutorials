package pl.kmaci3k.java.fx.tutorial;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;



/**
 * Created by mklys on 29/05/16.
 */
public class Main extends Application {

    /**
     * Terminology:
     * Old:             JavaFx:
     * Window           Stage
     * Content          Scene
     *
     */

    private Stage window;
    private BorderPane layout;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - tutorial 22");

        // File Menu
        Menu fileMenu = new Menu("_File");

        // Menu items
        MenuItem newFile = new MenuItem("_New...");
        newFile.setOnAction(event -> System.out.println("Create a new file"));

        fileMenu.getItems().add(newFile);
        fileMenu.getItems().add(new MenuItem("Open..."));
        fileMenu.getItems().add(new MenuItem("Save..."));
        fileMenu.getItems().addAll(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Settings"));
        fileMenu.getItems().addAll(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Exit"));

        // Edit Menu
        Menu editMenu = new Menu("_Edit");
        editMenu.getItems().add(new MenuItem("Cut"));
        editMenu.getItems().add(new MenuItem("Copy"));

        MenuItem pasteItem = new MenuItem("Paste");
        pasteItem.setOnAction(event -> System.out.println("Pasting sth"));
        pasteItem.setDisable(true);
        editMenu.getItems().add(pasteItem);



        // Main menu bar
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, editMenu);

        layout = new BorderPane();
        layout.setTop(menuBar);

        Scene scene = new Scene(layout, 300, 300);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
